# audio

https://vitux.com/how-to-control-audio-on-the-debian-command-line/

apt install install alsa-utils

alsamixer

amixer --help

amixer scontrols

Examples:

Examples

1. The following command will set the volume on the Master control/property of the first sound card to 100%

$ amixer -c 0 set Master 100%

2. The following command will set the volume on the Speaker control/property of the second sound card to 30%

$ amixer -c 1 set Speaker 50%

3. The following command will set the volume on the Speaker control/property of the second sound card to 3db

$ amixer -c 1 set Speaker 3db

4. The following command will increase the volume on the Speaker control/property of the second sound card by 2db

$ amixer -c 1 set Speaker 2db+

5. Use the following commands to mute and unmute a property.

$ amixer -c 0 set Mic mute

Or,

$ amixer -c 0 set Mic unmute

## icecast2 butt

https://icecast.org/docs/icecast-trunk/config_file/

https://www.michaeloland.com/2019/03/configuring-icecast-and-butt.html
